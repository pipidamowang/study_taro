const mockTeachDataAll = [
  {
    useName: "张丽",
    userSubTitle: ["高级副总裁", "培养总监"],
    teachZZ: "教师资质：20191100142008608", //教师资质
    teachHis: "2015.04-至今 安德鲁森中国公司就业创业部培训总监",
    teachPoint:
      "寓教于乐，逻辑清晰。方法实用，接地气。集美貌与才华于一身，专业功底扎实。",
  },
  {
    useName: "李四",
    userSubTitle: ["高级副总裁", "培养总监"],
    teachZZ: "教师资质：20191100142008608", //教师资质
    teachHis: "2015.04-至今 安德鲁森中国公司就业创业部培训总监",
    teachPoint:
      "寓教于乐，逻辑清晰。方法实用，接地气。集美貌与才华于一身，专业功底扎实。",
  },
  {
    useName: "王五",
    userSubTitle: ["高级副总裁", "培养总监"],
    teachZZ: "教师资质：20191100142008608", //教师资质
    teachHis: "2015.04-至今 安德鲁森中国公司就业创业部培训总监",
    teachPoint:
      "寓教于乐，逻辑清晰。方法实用，接地气。集美貌与才华于一身，专业功底扎实。",
  },
  {
    useName: "赵柳",
    userSubTitle: ["高级副总裁", "培养总监"],
    teachZZ: "教师资质：20191100142008608", //教师资质
    teachHis: "2015.04-至今 安德鲁森中国公司就业创业部培训总监",
    teachPoint:
      "寓教于乐，逻辑清晰。方法实用，接地气。集美貌与才华于一身，专业功底扎实。",
  },
];
export default mockTeachDataAll;
