const courseListData = {
  公开课: [
    {
      hotCount: 188,
      type: "机构",
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/de/17/708533671.jpg",
    },
    {
      hotCount: 188,
      type: "机构",
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/a3/6b/178703256.jpg",
    },
    {
      hotCount: 188,
      type: "机构",
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/61/8e/676056109.jpg",
    },
    {
      hotCount: 188,
      type: "机构",
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/9e/14/568728978.jpg",
    },
  ],
  优秀讲师: [
    {
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/de/17/708533671.jpg",
    },
    {
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/a3/6b/178703256.jpg",
    },
    {
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/61/8e/676056109.jpg",
    },
    {
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/9e/14/568728978.jpg",
    },
  ],
  推荐课程: [
    {
      hotCount: 188,
      type: "机构",
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/de/17/708533671.jpg",
    },
    {
      hotCount: 188,
      type: "机构",
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/a3/6b/178703256.jpg",
    },
    {
      hotCount: 188,
      type: "机构",
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/61/8e/676056109.jpg",
    },
    {
      hotCount: 188,
      type: "机构",
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/9e/14/568728978.jpg",
    },
    {
      hotCount: 188,
      type: "机构",
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/3a/7e/599193262.jpg",
    },
    {
      hotCount: 188,
      type: "机构",
      message: "中式面点师考证课程【好利来教育培训】",
      classSrc: "https://img1.droitstock.com/smallT/a5/7b/526497056.jpg",
    },
  ],
  头条信息: [
    {
      mesTitle: "全国教育工作会议召开，传递这些重要信息！",
      mesContent:
        " 1月7日至8日，2021年全国教育工作会议在京召开。会议强调，要...",
      mesTime: "2021-03-08",
      classSrc: "https://img1.droitstock.com/smallT/a5/7b/526497056.jpg",
    },
    {
      mesTitle: "全国教育工作会议召开，传递这些重要信息！",
      mesContent:
        " 1月7日至8日，2021年全国教育工作会议在京召开。会议强调，要...",
      mesTime: "2021-03-08",
      classSrc: "https://img1.droitstock.com/smallT/a5/7b/526497056.jpg",
    },
    {
      mesTitle: "全国教育工作会议召开，传递这些重要信息！",
      mesContent:
        " 1月7日至8日，2021年全国教育工作会议在京召开。会议强调，要...",
      mesTime: "2021-03-08",
      classSrc: "https://img1.droitstock.com/smallT/a5/7b/526497056.jpg",
    },
  ],
};
export default courseListData;
