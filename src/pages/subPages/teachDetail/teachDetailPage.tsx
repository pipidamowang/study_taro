import { View, Image, Text } from "@tarojs/components";
import Taro, { getCurrentInstance, useDidShow } from "@tarojs/taro";

import * as React from "react";
import "./teach_detail_style.css";
import leftPng from "../../../assets/icon_on_the_left.png";
import ClassItem from "../../../components/ClassItem";
import courseListData from "../../../mock/courseListData";

import mockTeachDataAll from "../../../mock/mockTeachDataAll";

function TeachDetailPage() {
  const [mockTeachData, setmockTeachData] = React.useState<any>({});
  React.useEffect(() => {
    Taro.hideHomeButton({
      success: (e) => {
        console.log(e);
      },
      fail:(e)=>{
        console.log(e);
      }
    });
    const todo = () => {
      const id: any = getCurrentInstance().router?.params?.id;
      setmockTeachData(mockTeachDataAll[id]);
    };
    todo();
  }, []);

  useDidShow(() => {
    Taro.showLoading({
      title: "加载中",
      icon: "loading",
      duration: 1000,
    });
    
  });

  return (
    <View className='teachDetailView'>
      <View className='teachDetailTopBack'>
        <Image
          src={leftPng}
          className='teachDetailBaclImg'
          onClick={() => {
            Taro.navigateBack();
          }}
        />
      </View>
      <View className='teachDetail_headWrap'>
        <View className='teachDetail_headMess'>
          <View className='teachDetail_headContain'>
            <Image src='https://jdc.jd.com/img/200' className='headerSize' />
          </View>
          <View className='teachDetail_rightView'>
            <View className='teachDetail_name'>{mockTeachData?.useName}</View>
            <View className='teachDetail_titleWrap'>
              {mockTeachData?.userSubTitle?.map((el: any, index: number) => (
                <View className='teachDetail_title' key={"title" + index}>
                  {el}
                </View>
              ))}
            </View>
          </View>
        </View>

        <View>
          <View className='teachDetail_userDetailWarp'>
            <View className='teachDetail_userTitle'>教师资质</View>
            <View className='teachDetail_userMessgae'>
              {mockTeachData?.teachZZ}
            </View>
          </View>

          <View className='teachDetail_userDetailWarp'>
            <View className='teachDetail_userTitle'>教育经历</View>
            <View className='teachDetail_userMessgae'>
              {mockTeachData?.teachHis}
            </View>
          </View>
          <View className='teachDetail_userDetailWarp'>
            <View className='teachDetail_userTitle'>教学特点</View>
            <View className='teachDetail_userMessgae'>
              {mockTeachData?.teachPoint}
            </View>
          </View>
        </View>
      </View>

      <View className='teachDetail_DetailClass'>
        <Text>相关推荐</Text>
        <View>
          <ClassItem tabData={courseListData["公开课"]} flag={false} />
        </View>
      </View>
    </View>
  );
}

export default TeachDetailPage;
