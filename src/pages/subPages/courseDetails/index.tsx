import { View, Video, ScrollView, Text, Image } from "@tarojs/components";

import React from "react";

import { AtTabs, AtTabsPane, AtButton } from "taro-ui";
import "./index.css";

import CourseListView from "./components/courseListView";
import sportPng from "../../../assets/sport.png";
import ClassItem from "../../../components/ClassItem";
import PageBtmInput from "../../../components/PageBtmInput";
import MyCommits from "../../../components/MyCommits";

import { commitsData } from "../../../mock/commitsData";

import courseListData from "../../../mock/courseListData";

const tabList = [{ title: "课程" }, { title: "课件" }, { title: "评论(127)" }];

export default function CourseDetails() {
  const [current, setCurrent] = React.useState<number>(0);
  function handleClick(value) {
    setCurrent(value);
  }
  return (
    <View className='courseView'>
      <View className='courseView_topVideo'>
        <Video
          style={{ height: "100%", width: "100%" }}
          src='https://wxsnsdy.tc.qq.com/105/20210/snsdyvideodownload?filekey=30280201010421301f0201690402534804102ca905ce620b1241b726bc41dcff44e00204012882540400&bizid=1023&hy=SH&fileparam=302c020101042530230204136ffd93020457e3c4ff02024ef202031e8d7f02030f42400204045a320a0201000400'
          controls
          objectFit='cover'
          autoplay={false}
          poster='https://misc.aotu.io/booxood/mobile-video/cover_900x500.jpg'
          id='video'
          enablePlayGesture
          loop={false}
          muted={false}
        />
      </View>
      {/*  */}
      <View>
        <AtTabs current={current} tabList={tabList} onClick={handleClick}>
          <AtTabsPane current={current} index={0}>
            <ScrollView>
              <View className='courseView_classView'>
                <View className='courseView_classTitle'>
                  <View className='courseView_titleSize'>
                    烘焙培训【安德鲁森培训】
                  </View>
                  <View style={{ color: "rgba(12, 177, 255, 100)" }}>
                    课程咨询
                  </View>
                </View>
                <View className='courseView_messageView'>
                  <View
                    style={{ color: "#E06833" }}
                    className='courseView_item'
                  >
                    ￥168
                  </View>
                  <View className='courseView_item'>购买人数1280</View>
                  <View className='courseView_item'>观看人数1.2w</View>
                  <View className='courseItemHasBack courseView_item'>
                    职业技能
                  </View>
                  <View className='courseItemHasBack courseView_item'>
                    烘焙师
                  </View>
                </View>
                <View className='courseView_contentView'>
                  课程介绍：采用线上点播或直播培训，线下考证的模式进行学习，用户可免费观看3节课，也可进行单集点播以及购买全部课程。
                </View>
                <View className='courseView_buyBtn'>
                  <AtButton type='primary'>立即购买</AtButton>
                </View>
              </View>
              {/* 课程列表 */}
              <View className='courseView_listView'>
                <CourseListView />
              </View>
              {/* 老师介绍 */}
              <View className='courseTeacher_view'>
                <View className='courseTeacher_headerView'>
                  <Text>老师介绍</Text>
                  <View className='courseTeacher_userView'>
                    <View className='courseTeacher_userView_left'>
                      <Image
                        style={{ width: "100%", height: "100%" }}
                        src='https://s.cn.bing.net/th?id=ODL.8acd6796e431785e9623a48ef791095b&w=146&h=146&c=7&rs=1&qlt=80&dpr=2&pid=RichNav'
                      />
                    </View>
                    <View className='courseTeacher_userView_right'>
                      <Text style={{ marginBottom: "7px" }}>KK老师</Text>
                      <Text className='courseTeacher_userView_right_message'>
                        安德鲁森教育培训组长。5年烘焙经验，
                        烘焙面包造型设计，担任安德鲁森...
                      </Text>
                    </View>
                  </View>
                </View>
                <View className='courseTeacher_Pic'>
                  <View>
                    <Text className='courseView_titleSize'>课程详情</Text>
                  </View>
                  <View style={{ marginTop: "14px" }}>
                    {[1, 3, 4, 5]?.map((el: any, index: number) => {
                      return (
                        <Image
                          src={sportPng}
                          key={index + "img"}
                          lazyLoad
                          mode='scaleToFill'
                          style={{ width: "100%" }}
                        />
                      );
                    })}
                  </View>
                </View>
              </View>

              <View className='courseOtherView'>
                <Text>相关推荐</Text>
                <View>
                  <ClassItem tabData={courseListData["公开课"]} flag={false} />
                </View>
              </View>
            </ScrollView>
          </AtTabsPane>
          <AtTabsPane current={current} index={1}>
            <ScrollView>
              <View className='coursewareView'>
                <View>
                  <Text className='coursewareView_titleSize'>本节视频PPT</Text>
                </View>
                <View style={{ marginTop: "14px" }}>
                  {[1, 3, 4, 5]?.map((el: any, index: number) => {
                    return (
                      <Image
                        src={sportPng}
                        key={index + "img"}
                        lazyLoad
                        mode='scaleToFill'
                        style={{ width: "100%" }}
                      />
                    );
                  })}
                </View>
              </View>
            </ScrollView>
          </AtTabsPane>
          <AtTabsPane current={current} index={2}>
            <ScrollView>
              <View className='commitsView'>
                <MyCommits commitsData={commitsData} />
              </View>
            </ScrollView>
          </AtTabsPane>
        </AtTabs>
      </View>

      <View className='courseBtoInput'>
        <PageBtmInput placeholder='添加评论' />
      </View>
    </View>
  );
}
