import { View, Text } from "@tarojs/components";
import { useState, useCallback } from "react";

export default function CourseListView() {
  const changeSelect = (index) => {
    let _temp = [...mockData];
    _temp = _temp?.map((el: any, key: number) => {
      el.now = false;
      if (key === index) {
        el.now = true;
      }
      return el;
    });
    setMockData(_temp);
  };

  const oldData = [
    { title: "黄油曲奇饼干的", type: "免费", time: "1:11:11", now: true },
    { title: "日常早餐餐包面", type: "免费", time: "1:11:11", now: false },
    { title: "网红戚风杯子蛋", type: "购买", time: "1:11:11", now: false },
    { title: "经典浮云蛋糕卷", type: "购买", time: "1:11:11", now: false },
    { title: "经典轻芝士甜品", type: "免费", time: "1:11:11", now: false },
    { title: "经芝士甜品", type: "免费", time: "1:11:11", now: false },
  ];
  const [mockData, setMockData] = useState<any>(oldData);

  const [showAll, setShowAll] = useState<boolean>(false);

  const todoShowAll = useCallback(() => {
    if (showAll) {
      setMockData(oldData);
    } else {
      setMockData([
        ...oldData,
        ...[
          {
            title: "日常早餐餐包面",
            type: "免费",
            time: "1:11:11",
            now: false,
          },
          {
            title: "网红戚风杯子蛋",
            type: "购买",
            time: "1:11:11",
            now: false,
          },
          {
            title: "经典浮云蛋糕卷",
            type: "购买",
            time: "1:11:11",
            now: false,
          },
          {
            title: "经典轻芝士甜品",
            type: "免费",
            time: "1:11:11",
            now: false,
          },
        ],
      ]);
    }
    setShowAll(!showAll);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [showAll]);

  return (
    <View style={{ flex: 1 }} className='courseListView_box'>
      <View className='courseListView_titleView'>
        <View>
          <Text>课程列表</Text>
          <Text className='courseListView_messageSize'>（共10课）</Text>
        </View>
        <Text
          style={{ marginTop: "1px" }}
          className='courseListView_labelSize'
          onClick={todoShowAll}
        >
          {!showAll ? "展开全部" : "隐藏"}
        </Text>
      </View>

      {mockData?.map((el: any, index: number) => {
        return (
          <View
            key={index + "listView"}
            onClick={() => {
              changeSelect(index);
            }}
            className={[
              "courseListView_mainView",
              el?.now ? "courseListView_mainHasCheckView" : "",
            ].join(" ")}
          >
            <View className='courseListView_label'>
              <Text>
                {index + 1}.{el?.title}
              </Text>
            </View>
            <View className='courseListView_valueBox'>
              <View className='courseListView_typeView'>{el?.type}</View>
              {el?.now && (
                <View style={{ marginLeft: "-25px" }}>
                  <View className='at-icon at-icon-settings'></View>
                </View>
              )}
              <View className='courseListView_timeView'>{el?.time}</View>
            </View>
          </View>
        );
      })}
    </View>
  );
}
