import { View, Radio, RadioGroup, Input } from "@tarojs/components";

import { AtAvatar,AtButton } from "taro-ui";

import Taro from "@tarojs/taro";

function gotoback() {
  Taro.navigateBack();
}

const radioData = [
  {
    value: "1",
    text: "男",
    checked: true,
  },
  {
    value: "0",
    text: "女",
    checked: false,
  },
];

let userInfoData = {
  headerSrc: "https://jdc.jd.com/img/200",
  userName: "吴彦祖",
  userInfoPhone: "13213131312",
};

function UserDetail() {
  return (
    <View className='userViewCenter'>
      <View className='userViewBox'>
        <View className='userViewBox_itemBox'>
          <View>修改头像</View>
          <View>
            <AtAvatar circle image={userInfoData?.headerSrc}></AtAvatar>
          </View>
        </View>
        <View className='userViewBox_itemBox'>
          <View>修改昵称</View>
          <View style={{ textAlign: "end" }}>
            <Input type='text' value={userInfoData?.userName} />
          </View>
        </View>
        <View className='userViewBox_itemBox'>
          <View>设置性别</View>
          <View>
            <RadioGroup>
              {radioData.map((item, i) => {
                return (
                  <Radio
                    className='radio-list__radio'
                    style={{ marginLeft: "10px" }}
                    key={i}
                    value={item.value}
                    checked={item.checked}
                  >
                    {item.text}
                  </Radio>
                );
              })}
            </RadioGroup>
          </View>
        </View>
        <View className='userViewBox_itemBox'>
          <View>电话号码</View>
          <View style={{ textAlign: "end" }}>
            <Input type='phone' value={userInfoData?.userInfoPhone} />
          </View>
        </View>
      </View>
      <AtButton
        type='primary'
        size='normal'
        className='userViewBox_button'
        onClick={gotoback}
      >
        点击保存
      </AtButton>
    </View>
  );
}

export default UserDetail;
