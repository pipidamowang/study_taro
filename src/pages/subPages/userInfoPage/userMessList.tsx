import { View, Image, ScrollView } from "@tarojs/components";

import * as React from "react";

import { AtTabs, AtTabsPane } from "taro-ui";

import "./message_style.css";

const baseList = [{ title: "私信" }, { title: "活动" }];

const baseTabsList = [
  {
    messTitle: "系统通知",
    messTime: "2小时前",
    messContent: "终于要揭晓为你准备的【年末礼物】了！参与100%有奖",
  },
  {
    messTitle: "系统通知",
    messTime: "2小时前",
    messContent: "终于要揭晓为你准备的【年末礼物】了！参与100%有奖",
  },
  {
    messTitle: "系统通知",
    messTime: "2小时前",
    messContent: "终于要揭晓为你准备的【年末礼物】了！参与100%有奖",
  },
  {
    messTitle: "系统通知",
    messTime: "2小时前",
    messContent: "终于要揭晓为你准备的【年末礼物】了！参与100%有奖",
  },
];

function UserMessList() {
  const [current, setcurrent] = React.useState<number>(0);
  function handleClick(e) {
    setcurrent(e);
  }
  return (
    <View className='userSelfPageView messPageView'>
      <AtTabs current={current} scroll tabList={baseList} onClick={handleClick}>
        {baseList?.map((_: any, index: number) => (
          <AtTabsPane current={current} index={0} key={"mess" + index}>
            <ScrollView scrollY>
              <View className='messTabView'>
                {baseTabsList?.map((el: any, key: number) => (
                  <View className='messTabItemView' key={"tabsItem" + key}>
                    <Image
                      src='https://jdc.jd.com/img/200'
                      className='messTabItemViewLeft'
                    ></Image>
                    <View className='messTabItemViewRight'>
                      <View className='messTabItemViewRightTitle'>
                        <View className='title'>{el?.messTitle}</View>
                        <View className='time'>{el?.messTime}</View>
                      </View>
                      <View className='messTabItemViewRightMess'>
                        {el?.messContent}
                      </View>
                    </View>
                  </View>
                ))}
              </View>
            </ScrollView>
          </AtTabsPane>
        ))}
      </AtTabs>
    </View>
  );
}

export default UserMessList;
