import { View } from "@tarojs/components";
import { getCurrentInstance } from "@tarojs/taro";

import UserDetail from "./userDetail";

import UserMessList from "./UserMessList";
import "./user_info.css";

function UserInfo() {
  const params: any = getCurrentInstance()?.router?.params;
  function getTypePage() {
    switch (params?.type) {
      case "个人资料":
        return <UserDetail />;
      case "消息中心":
        return <UserMessList />;
      default:
        return <View>暂无数据</View>;
    }
  }
  return (
    <View className='userView'>
      <View className='userViewHeader'>{params?.type}</View>
      <View className='userViewBottom'>{getTypePage()}</View>
    </View>
  );
}

export default UserInfo;
