import { View, Image, Text } from "@tarojs/components";

import Taro from "@tarojs/taro";

import "./user_self.css";

import phonePng from "../../assets/dianhua.png";

import youPng from "../../assets/xiangyou.png";

import xiaoxiPng from "../../assets/xiaoxi.png";
import shoucangPng from "../../assets/shoucang.png";
import lishiPng from "../../assets/lishi.png";
import dingdanPng from "../../assets/dingdan.png";

//

import homePng from "../../assets/home.png";
import cardPng from "../../assets/card.png";
import fankuiPng from "../../assets/fankui.png";
import systemPng from "../../assets/system.png";

const iconListData = [
  {
    title: "消息中心",
    class: "",
    imgSrc: xiaoxiPng,
    textClass: "",
  },
  {
    title: "我的订单",
    class: "dingdanClass",
    imgSrc: dingdanPng,
    textClass: "",
  },
  {
    title: "我的收藏",
    class: "shoucangClass",
    imgSrc: shoucangPng,
    textClass: "shoucangText",
  },
  {
    title: "学习足迹",
    class: "lishiClass",
    imgSrc: lishiPng,
    textClass: "",
  },
];

const setListData = [
  {
    title: "关于我们",
    imgSrc: homePng,
  },
  {
    title: "我的卡券",
    imgSrc: cardPng,
  },
  {
    title: "反馈意见",
    imgSrc: fankuiPng,
  },
  {
    title: "系统设置",
    imgSrc: systemPng,
  },
];

const objTitleType = {
  icon0: "消息中心",
  icon1: "我的订单",
  icon2: "我的收藏",
  icon3: "学习足迹",
  system0: "关于我们",
  system1: "我的卡券",
  system2: "反馈意见",
  system3: "系统设置",
};

const url =
  "https://s.cn.bing.net/th?id=ODL.8acd6796e431785e9623a48ef791095b&w=146&h=146&c=7&rs=1&qlt=80&dpr=2&pid=RichNav";

export default function UserSelfPage() {
  function gointoUserInfo() {
    Taro.navigateTo({
      url: "/pages/subPages/userInfoPage/userInfo?type=个人资料",
    });
  }

  function clickIcon(index) {
    Taro.navigateTo({
      url: "/pages/subPages/userInfoPage/userInfo?type=" + objTitleType[index],
    });
  }
  return (
    <View className='userSelfPageView'>
      <View className='userSelfPageView_top'>
        <View className='userSelfPageView_userHeader'>
          <View className='userSelfPageView_userView' onClick={gointoUserInfo}>
            <View className='userSelfPageView_userView_left'>
              <Image style={{ width: "100%", height: "100%" }} src={url} />
            </View>
            <View className='userSelfPageView_userView_right'>
              <Text className='userSelfPageView_userView_right_userName'>
                KK老师
              </Text>
              <View className='userSelfPageView_userView_right_message'>
                <Image
                  src={phonePng}
                  className='userSelfPageView_userView_right_Icon'
                />
                182****0894
              </View>
            </View>
            <View>
              <Image src={youPng} className='userSelfPageView_rightIcon' />
            </View>
          </View>
        </View>
        <View className='userSelfPageView_otherIconView'>
          {iconListData?.map((el, index) => (
            <View
              className='userSelfPageView_iconViewItem'
              key={index + "icon"}
              onClick={() => {
                clickIcon("icon" + index);
              }}
            >
              <View className='userSelfPageView_iconViewContent'>
                <Image
                  className={["userSelfPageView_rightIcon", el?.class].join(
                    " "
                  )}
                  src={el?.imgSrc}
                />
              </View>
              <View
                className={[
                  "userSelfPageView_iconViewContent",
                  el?.textClass,
                ].join(" ")}
              >
                {el?.title}
              </View>
            </View>
          ))}
        </View>
      </View>
      <View className='userSelfPageView_bottom'>
        <View className='userSelfPageView_bottomTitle'>常用工具</View>

        <View className='userSelfPageView_bottomCenter'>
          {setListData?.map((el, index) => (
            <View
              className='userSelfPageView_iconViewItem'
              key={index + "system"}
              onClick={() => {
                clickIcon("system" + index);
              }}
            >
              <View className='userSelfPageView_iconViewContent'>
                <Image
                  className='userSelfPageView_rightIcon'
                  src={el?.imgSrc}
                />
              </View>
              <View className='userSelfPageView_iconViewContent'>
                {el?.title}
              </View>
            </View>
          ))}
        </View>
      </View>
      <View className='userSelfPageView_empty'></View>
    </View>
  );
}
