import { View, ScrollView, CoverView, CoverImage } from "@tarojs/components";
import { Component } from "react";
import Taro from "@tarojs/taro";
import { AtInput, AtTabs, AtTabsPane } from "taro-ui";
import QgSwiper from "../../components/Swiper";
import "./index.css";
import erweima from "../../assets/erweima.png";
import sousuo from "../../assets/sousuo.png";
import TabItem from "../../components/MyTabItem";
import courseListData from "../../mock/courseListData";

const tabList = [
  { title: "公开课" },
  { title: "优秀讲师" },
  { title: "推荐课程" },
  { title: "头条信息" },
];
class Index extends Component {
  state = {
    value: "",
    current: 0,
    startSticky: false,
  };
  todo = () => {
    Taro.navigateTo({ url: "/pages/subPages/other/index" });
  };

  handleChange = () => {};
  handleClick(value) {
    this.setState({
      current: value,
    });
  }
  onPageScroll = (e) => {
    if (e.scrollTop >= 250) {
      this.setState({ startSticky: true });
    } else {
      this.setState({ startSticky: false });
    }
    console.log(this.state.startSticky);
  };
  render() {
    return (
      <ScrollView className='page_styled' onScroll={this.onPageScroll}>
        <View className='page_top_styled'>
          <View className='page_swiper'>
            <QgSwiper />
          </View>
          <CoverView className='top_input_search'>
            <CoverView className='top_input_box'>
              <CoverImage src={sousuo} className='search_icon searchIcon' />
              <AtInput
                clear
                type='text'
                placeholder='搜索课程/教师/文章'
                value={this.state.value}
                onChange={this.handleChange}
              >
                <CoverView className='erweimaIcon'>
                  <CoverImage src={erweima} className='search_icon ' />
                </CoverView>
              </AtInput>
            </CoverView>
          </CoverView>
        </View>
        <View
          className={[
            "page_bottom_styled",
            this.state.startSticky ? "startStickyStyle" : "",
          ].join(" ")}
        >
          <AtTabs
            current={this.state.current}
            scroll
            tabList={tabList}
            onClick={this.handleClick.bind(this)}
            swipeable={false}
          >
            {tabList?.map((el: any, index: number) => {
              return (
                <AtTabsPane
                  current={this.state.current}
                  index={index}
                  key={index + "tabs"}
                >
                  <ScrollView scrollWithAnimation>
                    <View>
                      <TabItem
                        tabData={courseListData[el?.title]}
                        baseType={el?.title}
                      />
                    </View>
                  </ScrollView>
                </AtTabsPane>
              );
            })}
          </AtTabs>
        </View>
      </ScrollView>
    );
  }
}

export default Index;
