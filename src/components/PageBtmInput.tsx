import { View, Input } from "@tarojs/components";
import { useState } from "react";
import { AtInput } from "taro-ui";

import "./page_btm_input.css";

export default function PageBtmInput({ placeholder }: any) {
  const [value, setValue] = useState<any>(undefined);
  function handleChange(temp) {
    setValue(temp);
  }
  return (
    <View className='pageBtmInputView'>
      <AtInput
        name='value'
        type='text'
        placeholder={placeholder}
        value={value}
        onChange={handleChange}
      />

      <View className='pageBtmInputView_right' style={{ marginLeft: "30px" }}>
        <View className='pageBtmInputView_right'>
          <View className='at-icon at-icon-star iconSize' />
          <View className='textView'>20k</View>
        </View>
        <View className='pageBtmInputView_right' style={{ marginLeft: "30px" }}>
          <View className='at-icon at-icon-external-link iconSize' />
          <View className='textView'>20k</View>
        </View>
      </View>
    </View>
  );
}
