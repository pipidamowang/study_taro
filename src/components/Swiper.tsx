import { Swiper, SwiperItem, Image } from "@tarojs/components";

const swiperArr = [
  "https://img1.droitstock.com/smallT/46/89/847851913.jpg",
  "https://img1.droitstock.com/smallT/7c/b7/474935566.jpg",
  "https://img1.droitstock.com/smallT/e8/a6/850065434.jpg",
];

function QgSwiper() {
  return (
    <Swiper
      style={{ height: "100%" }}
      indicatorColor='#999'
      indicatorActiveColor='#333'
      vertical={false}
      circular
      indicatorDots
      autoplay
    >
      {swiperArr?.map((el: any, index: number) => (
        <SwiperItem key={index + "swiper"}>
          <Image
            src={el}
            mode='scaleToFill'
            style={{ width: "100%", height: "100%" }}
          />
        </SwiperItem>
      ))}
    </Swiper>
  );
}

export default QgSwiper;
