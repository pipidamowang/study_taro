import { View, Image } from "@tarojs/components";

import Taro from "@tarojs/taro";
import "./class_item.css";

import hotPng from "../assets/hot.png";

export default function ClassItem({ flag = true, tabData = [] }: any) {
  const gotoDetail = () => {
    Taro.navigateTo({ url: "/pages/subPages/courseDetails/index" });
  };
  return (
    <View className='tabWrapView'>
      {tabData?.map((el: any, index: any) => (
        <View onClick={gotoDetail} key={index + "itemDetail"}>
          <View
            className='tabItemView'
            style={{ backgroundImage: `url(${el?.classSrc})` }}
          >
            <View className='tabItemView_title'>
              <View className='tabItemView_text tabItemView_text_left'>
                <Image src={hotPng} className='tabItemView_iconImg' />
                <View>{el?.hotCount}</View>
              </View>
              {flag && (
                <View className='tabItemView_text tabItemView_text_right'>
                  {el?.type}
                </View>
              )}
            </View>
          </View>
          <View className='tabItemView_name'>{el?.message}</View>
        </View>
      ))}
    </View>
  );
}
