import { View, Image } from "@tarojs/components";

import "./message_item_style.css";

function MessageItem({ tabData }) {
  return (
    <View className='mssageItemView'>
      {tabData?.map((el: any, index: number) => (
        <View className='mssageItemViewBox' key={"message" + index}>
          <View className='mssageItemTitle'>{el?.mesTitle}</View>
          <View className='mssageItemCenter'>
            <View className='mssageItemCenter_left'>
              <View className='mssageItemCenter_leftTop'>{el?.mesContent}</View>
              <View className='mssageItemCenter_leftBottom'>{el?.mesTime}</View>
            </View>
            <View className='mssageItemCenter_right'>
              <Image src={el?.classSrc} className='mssageItemCenter_img' />
            </View>
          </View>
        </View>
      ))}
    </View>
  );
}

export default MessageItem;
