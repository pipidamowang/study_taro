import { View } from "@tarojs/components";
import Taro from "@tarojs/taro";

import "./teach_item_style.css";

function TeacherBarItem({ tabData }) {
  return (
    <View className="teachItemView">
      {tabData?.map((el: any, index: number) => (
        <View
          className="teachItem_warp"
          key={"teach" + index}
          onClick={() => {
            Taro.navigateTo({
              url: "/pages/subPages/teachDetail/teachDetailPage?id=" + index,
            });
          }}
        >
          <View
            style={{ backgroundImage: `url(${el?.classSrc})` }}
            className="teachItem_warp_img"
          ></View>
          <View className="teachItem_text">{el?.message}</View>
        </View>
      ))}
    </View>
  );
}

export default TeacherBarItem;
