import { View } from "@tarojs/components";
import { AtAvatar } from "taro-ui";

import "./my_commits.css";

export default function MyCommits({ commitsData }: any) {
  const renderOtherPresCommits = (item: any) => {
    return (
      <View className='my_commitsView_bottom'>
        <View className='my_commitsView_bottom_userCommits'>
          {item?.replyUser}：{item?.replyMess}
        </View>
      </View>
    );
  };

  return (
    <View className='my_commitsView_box'>
      {commitsData?.map((el: any, index: number) => (
        <View className='my_commitsView' key={"commits" + index}>
          <View className='my_commitsView_top'>
            <View>
              <AtAvatar
                circle
                image={el?.headerSrc || "https://jdc.jd.com/img/200"}
              />
            </View>
            <View className='my_commitsView_top_right'>
              <View className='my_commitsView_top_right_name'>
                {el?.username}
              </View>
              <View className='my_commitsView_top_right_textView'>
                {el?.replyMess}
              </View>
              <View className='my_commitsView_top_otherView'>
                <View className='my_commitsView_otherView_time'>
                  {el?.replyTime}
                </View>
                <View className='my_commitsView_otherView_other'>
                  <View className='my_commitsView_otherView_iconRight'>
                    <View className='at-icon at-icon-heart'></View>
                    <View>{el?.replyCount}</View>
                  </View>
                  <View className='at-icon at-icon-message'></View>
                </View>
              </View>
            </View>
          </View>
          {el?.replyUser && renderOtherPresCommits(el)}
        </View>
      ))}
    </View>
  );
}
