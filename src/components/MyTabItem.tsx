import { View } from "@tarojs/components";
import ClassItem from "../components/ClassItem";
import TeacherBarItem from "../components/TeacherBarItem";
import MessageItem from "../components/MessageItem";

export default function TabItem({ tabData = [], baseType }: any) {
  const renderDetails = () => {
    if (baseType === "公开课") {
      return <ClassItem tabData={tabData} />;
    } else if (baseType === "优秀讲师") {
      return <TeacherBarItem tabData={tabData} />;
    } else if (baseType === "推荐课程") {
      return <ClassItem tabData={tabData} />;
    } else if (baseType === "头条信息") {
      return <MessageItem tabData={tabData} />;
    } else {
      return <View>暂无数据</View>;
    }
  };
  return <View>{renderDetails()}</View>;
}
