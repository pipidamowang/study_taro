export default {
  pages: [ "pages/index/index","pages/userSelf/index"],
  subpackages: [
    {
      root: "pages/subPages",
      name: "subPages",
      pages: [
        "nodes/index",
        "other/index",
        "courseDetails/index",
        "teachDetail/teachDetailPage",
        "userInfoPage/userInfo"
      ],
    },
  ],
  tabBar: {
    list: [
      {
        iconPath: "resource/hotest.png",
        selectedIconPath: "resource/hotest_on.png",
        pagePath: "pages/index/index",
        text: "首页",
      },
      {
        iconPath: "resource/latest.png",
        selectedIconPath: "resource/lastest_on.png",
        pagePath: "pages/userSelf/index",
        text: "个人",
      },
    ],
    color: "#ccc",
    selectedColor: "#56abe4",
    backgroundColor: "#fff",
    borderStyle: "white",
  },
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "WeChat",
    navigationBarTextStyle: "black",
  },
};
